function [ energy ] = imenergy( imgL, imgR, fn )
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

if(strcmp(fn, 'grad')) %not implemented
    imgL = im2double(imgL);
    imgL = rgb2gray(imgL);
    energy = imgradient(imgL);
elseif(strcmp(fn, 'ssd'))
    energy = abs(imgL(:,:,:)-imgR(:,:,:)).^2;
    energy = energy(:,:,1) + energy(:,:,2) + energy(:,:,3);
end