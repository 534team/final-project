int minInLastRow(double* d, int rows, int cols);

double valMinOf3(double*d, int in1, int in2, int in3);

double colMinOf3(double*d, int row, int rows, int col1, int col2, int col3);

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]);

mxArray* vertDynamicMap(const mxArray* d);

double valMinOf3(double*d, int in1, int in2, int in3);

double minValInLastRow(double* d, int rows, int cols);

/*http://classes.soe.ucsc.edu/ee264/Fall11/cmex.pdf*/
void DisplayMatrix(char *Name, double *Data, int M, int N); 