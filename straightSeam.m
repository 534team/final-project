function [seam, seamEnergy] = straightSeam( energy )
%STRAIGHTSEAM finds the best straight vertical seam in energy

i = 1;
[row,col] = size(energy);
minSeamVal = 999999999;
minSeamIndex = -1;
while(i<=col)
    if(sum(energy(:,i))<minSeamVal)
       minSeamVal = sum(energy(:,i));
       minSeamIndex = i;
    end
    i = i + 1;
end

seam = ones(row,1) * minSeamIndex;
seamEnergy = minSeamVal;
end

