function seam = horizontal_seam( energy )
%HORIZONTAL SEAM returns a least resistance seam (an array of pixles) where
%the index is the col number and the value is the row number of the pixle in the seam

%convert just incase
[rows, cols, ~] = size(energy);
dynamicMap = zeros(rows, cols);

i = 1;
while(i <= cols)
    j = 1;
    while(j <= rows)
        if(i == 1) %bootstrap first row
            dynamicMap(j,i) = energy(j,i);
        else 
            if(j == 1)
                jminus1 = 1;
                jplus1 = j + 1;
            elseif (j == rows)
                jminus1 = j -1;
                jplus1 = rows; 
            else
                jminus1 = j - 1;
                jplus1 = j + 1;
            end
            
            dynamicMap(j,i) = energy(j,i) + min([dynamicMap(j,i-1), dynamicMap(jplus1,i-1), dynamicMap(jminus1,i-1)]);
        end 
        j = j + 1;
    end
    i = i + 1;
end

if(nargin == 2) %this is for exp 2
   seam = dynamicMap;
   return
end

seam = zeros(1,cols);

i = cols;
while(i >= 1)
    if(i == cols)
        lastCol = dynamicMap(:,i);
        smallest = 2; %bootstrapping
    else
        if(smallest == 1) 
            smallestminus = 1;
            smallestplus = 2;
        elseif (smallest == rows)
            smallestminus = rows - 1;
            smallestplus = rows;
        else
            smallestminus = smallest - 1;
            smallestplus = smallest + 1;
        end
        lastCol = dynamicMap([smallestminus,smallest,smallestplus],i);
    end
    
    [~,temp] = min(lastCol);
    
    if (smallest == 1)
        if(temp ~= 1) %if smallest = 1, then leave smallest
            smallest = temp + (smallest -2);
        end
    elseif (smallest == rows)
        if(temp ~= 3) %if smallest ==3 then leave smallest
            smallest = temp + (smallest -2);
        end
    else
        smallest = temp + (smallest -2);
    end
    
    seam(i) = smallest;
    i = i - 1;
end