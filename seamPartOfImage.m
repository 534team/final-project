function [left, right, error ] = seamPartOfImage(fid, in, overlapPercent, offset)
%SEAMPARTOFIMAGE in is the input image. if overlap percent ==1 then we run strictly off ofset, otherwise we start at overlap percent and the add offset. the second is more of a remanant.

%
% -------
% | | | |
% |R| |L|
% | | | |
% -------
%

[~,col, ~] = size(in);
if(overlapPercent < 1) 
	version = 'percent';
else 
	version= 'pixle';
end

if(strcmp(version, 'percent'))
	if((floor(col*(overlapPercent)) + offset) > floor(.5*col) || floor(col*(overlapPercent)) + offset < 4)
		 left = 0;
		 right = 0;
		 error = true;
		 return;
	end

	startl = floor(col*(1 - overlapPercent)) + offset;
	endr = col - (startl-1);

	left = in(:,(startl:1:col),:);
	right = in(:,(1:1:endr),:);
	error = false;
elseif(strcmp(version,'pixle'))
	if(offset < floor(.5*col) || offset > (col - 4))
		 left = 0;
		 right = 0;
		 error = true;
		 return;
    end
    
	startl = offset;
	endr = col - (startl-1);
    fprintf(fid,'looking at seam with width: %f\n',endr);
    
	left = in(:,(startl:1:col),:);
	right = in(:,(1:1:endr),:);
	error = false;
else 
	left = 0;
	right = 0;
	error= true;
end
end