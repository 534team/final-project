function rotatedImg = imRotate( inImage, numOf90s )
%IMROTATE rotates image 90*numOf90s degrees
%   splits each layer, roates each layer, combines layers

red = inImage(:,:,1);
green = inImage(:,:,2);
blue = inImage(:,:,3);
red = rot90(red, numOf90s);
green = rot90(green, numOf90s);
blue = rot90(blue, numOf90s);
rotatedImg = cat(3, red,green,blue);

end

