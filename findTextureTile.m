function out = findTextureTile( in )

fid = fopen('data.log','w+');
fprintf(fid, 'Starting texture-tile generation\n');

% setup stuff
in = im2double(in);

% TEST PARARMS %
ReduceFactor = 1; %must be less than .5 or 1
EnergyMode = 'ssd'; %currently only option
SeamMode = 'straight'; %could be '8-con' or 'straight'

fprintf(fid, 'reduce factor: %f\n', ReduceFactor);
fprintf(fid, 'energy mode: %s\n', EnergyMode);
fprintf(fid, 'seam mode: %s\n', SeamMode);

bestMinEnergy = 999999999;
while(true) %keep running incase next iteration is better! 
    [seam, ewidth1, minEnergy1] = findBestSeam(fid, in, ReduceFactor, EnergyMode, SeamMode);
    out = squareImageAtSeam(fid, in, ewidth1, seam);
    minEnergy1 = minEnergy1 / size(seam, 1);
    
    fprintf(fid, 'flipping and repeating\n');
    outFlipped = imRotate(out, 1);
    [seam, ewidth2, minEnergy2] = findBestSeam(fid, outFlipped, ReduceFactor, EnergyMode, SeamMode);
    outFlippedSquared = squareImageAtSeam(fid, outFlipped, ewidth2, seam);
    minEnergy2 = minEnergy2 / size(seam, 1);
    
    outFinal = imRotate(outFlippedSquared,3);
    if((minEnergy1 + minEnergy2) < bestMinEnergy)
        bestMinEnergy = (minEnergy1 + minEnergy2);
        oldOutFinal = outFinal;
        in = outFinal;
        fprintf(fid, 're-running (best energy was: %f)\n',bestMinEnergy);
    else
        fprintf(fid, 're-run was bad (worse energy of: %f), reverting to previous.\n',(minEnergy1 + minEnergy2));
        out = oldOutFinal;
        fclose(fid);
        return
    end
end


end

