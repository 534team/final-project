#include "mex.h"
#include "vertSeam.h"

/*
 *(i,j)     [(i-1)+rows*(j-1)]
 *
 *(i, j-1)  [(i-1)+rows*(j-2)]
 *(i, j+1)  [(i-1)+rows*(j)]
 *
 *(i-1,j)   [(i-2)+rows*(j-1)]
 *(i+1,j)   [(i)+rows*(j-1)]
 *
 *(i-1,jminus1) [(i-2)+rows*(jminus1-1)]
 *(i-1,jplus1)  [(i-2)+rows*(jplus1-1)]
 */

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
    bool debug = false;
    
    if(nlhs != 0 && nlhs != 2) {
        mexPrintf("Only outputs 2 argument, you have: %d\n",nlhs);
        return;
    }
    
    if(nrhs != 1) {
        mexPrintf("Only takes 1 argument\n");
        return;
    }
    
    if(mxGetNumberOfDimensions(prhs[0]) != 2) {
        mexPrintf("input must be 2D\n");
        return;
    }

    /*SET UP VARIABLES*/
    const mxArray* energy = prhs[0];
    int rows = mxGetM(energy);
    int cols = mxGetN(energy);
    mxArray* dynamicMap = vertDynamicMap(energy); /*array*/
    
    if(debug) { DisplayMatrix("map", mxGetPr(dynamicMap), rows, cols); }
    
    double* d = mxGetPr(dynamicMap); /*data pointer*/
    mxArray* seam = mxCreateDoubleMatrix(rows, 1, mxREAL); /*array zeros(1,cols);*/
    double* s = mxGetPr(seam);     /*data pointer*/
    
    int i = rows;
    int col, col1, col2, col3;   
    while(i > 0) {
        if(i == rows) {
            s[i-1] = minInLastRow(d, rows, cols) + 1; /*bootstrap*/
            col = s[i-1] -1;
        } else {
            if(col == 0) {/*no minus option*/
                col1 = 0;
                col2 = 0;
                col3 = 1;
            } else if(col == cols) { /*no plus option*/
                col1 = cols - 1;
                col2 = cols;
                col3 = cols;
            } else { /*normal*/
                col1 = col - 1;
                col2 = col;
                col3 = col + 1;
            }
            s[i-1] = colMinOf3(d,rows,i,col1,col2,col3) +1;
            col = s[i-1] -1;
        }
        i--;
    }
   
    plhs[0] = seam;
    plhs[1] = mxCreateDoubleMatrix(1,1,mxREAL);
    double* p = mxGetPr(plhs[1]);
    p[0] = minValInLastRow(d,rows,cols);
    mxDestroyArray(dynamicMap);
    nlhs =2;
    /*Dont Destroy Seam!*/
    return;
}

int minInLastRow(double* d, int rows, int cols) {
    int i = 0;
    int col = 0;
    double min = DBL_MAX;
    while(i < cols) {
        if(d[(rows-1)+rows*(i)] <= min) {
            col = i;
            min = d[(rows-1)+rows*(i)];
        }
        i++;
    }
    return col;
}

double minValInLastRow(double* d, int rows, int cols) {
    int i = 0;
    double min = DBL_MAX;
    while(i < cols) {
        if(d[(rows-1)+rows*(i)] <= min) {
            min = d[(rows-1)+rows*(i)];
        }
        i++;
    }
    return min;
}

double valMinOf3(double*d, int in1, int in2, int in3) {
    /*dynamicMap(j,i) = energy(j,i) + min([dynamicMap(j,i-1), dynamicMap(jplus1,i-1), dynamicMap(jminus1,i-1)]);*/
    double min;
    if((d[in1] <= d[in2]) && (d[in1] <= d[in3])) {
        /*i-1,j*/
        min = d[in1];
    } else if((d[in2] <= d[in1]) && (d[in2] <= d[in3])) {
        /*i-1, j+1*/
        min = d[in2];
    } else {
        /*i-1, j-1*/
        min = d[in3];
    }
    return min;
}

double colMinOf3(double*d, int row, int rows, int col1, int col2, int col3) {
    /*dynamicMap(j,i) = energy(j,i) + min([dynamicMap(j,i-1), dynamicMap(jplus1,i-1), dynamicMap(jminus1,i-1)]);*/
    if((d[row + rows*col1] <= d[row + rows*col2]) && (d[row + rows*col1] <= d[row + rows*col3])) {
        return col1;
    } else if((d[row + rows*col2] <= d[row + rows*col1]) && (d[row + rows*col2] <= d[row + rows*col3])) {
        return col2;
    } else {
        return col3;
    }
}

mxArray* vertDynamicMap(const mxArray *energy) {
    int rows = mxGetM(energy);
    int cols = mxGetN(energy);
    
    mxArray* dynamicMap = mxCreateDoubleMatrix(rows, cols, mxREAL); /*array*/
    double* d = mxGetPr(dynamicMap); /*data pointer*/
    double* e = mxGetPr(energy);     /*data pointer*/
    int i = 1;
    int j, jminus1, jplus1;
    double min;
    
	while(i <= rows) {
        j = 1;
        while(j <= cols) {
            if(i == 1) { /*bootstrap first row*/
                d[(i-1)+rows*(j-1)] = e[(i-1)+rows*(j-1)];
                /*dynamicMap(i,j) = energy(i,j);*/
            } else {
                if(j == 1) {/*no minus option*/
                    jminus1 = 1;
                    jplus1 = j + 1;
                } else if(j == cols) { /*no plus option*/
                    jminus1 = j -1;
                    jplus1 = j; 
                } else { /*normal*/
                    jminus1 = j - 1;
                    jplus1 = j + 1;
                }

                min = valMinOf3(d,(i-2)+rows*(j-1), (i-2)+rows*(jplus1-1), (i-2)+rows*(jminus1-1));
                
                d[(i-1)+rows*(j-1)] = e[(i-1)+rows*(j-1)] + min;
            } 
            j++;
        }
        i++;
    }
    
    return dynamicMap;
}

void DisplayMatrix(char *Name, double *Data, int M, int N)
{ /* Display matrix data */
    int m, n;
    mexPrintf("%s = \n", Name);
    for(m = 0; m < M; m++){ 
        mexPrintf("\n");
        for(n = 0; n < N; n++){
            mexPrintf("%8.4f ", Data[m + M*n]);
        }
    }
    mexPrintf("\n");
}
