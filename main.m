%Philip Reasa
%906 502 7071

%use these bools for your convinience
xp1 = true;
xp2 = true;
xp3 = true;
xp4 = true;
xp5 = true;

%bring in the image(s)
providedIm = imread('union-terrace.jpg');
badIm = imread('reasa.5a.jpg');
goodIm = imread('reasa.4a.jpg');

%
%Experiment 1 - Get shrinking!
%
if(xp1)
shrink0_100 = shrnk(providedIm,0,100);
shrink100_0 = shrnk(providedIm,100,0);
shrink100_100 = shrnk(providedIm,100,100);

imwrite(shrink0_100, 'reasa.1a.jpg');
imwrite(shrink100_0, 'reasa.1b.jpg');
imwrite(shrink100_100, 'reasa.1c.jpg');
end

%
%Experiment 2 - Energy!
%
if(xp2)
energy = imenergy(providedIm);
handle = imagesc(energy);
saveas(handle, 'reasa.2a.jpg');

map = horizontal_seam(providedIm, 'stop early');
handle = imagesc(map);
saveas(handle, 'reasa.2b.jpg');
end

%
%Experiment 3 - overlay
%
if(xp3)
hSeam = horizontal_seam(providedIm);
overlay = overlay_seam(providedIm, hSeam);

%roate our images
providedIm = imRotate(providedIm,1);
overlay = imRotate(overlay,1);

%add new seam
vSeam = horizontal_seam(providedIm);
overlay = overlay_seam(overlay, vSeam);

%rotate them back
providedIm = imRotate(providedIm,3);
overlay = imRotate(overlay,3);

imwrite(overlay, 'reasa.3.jpg');
end

%
%Expiriment 4 - Good result
%

if(xp4)
    
result = shrnk(goodIm,40,40); 
imwrite(result, 'reasa.4b.jpg');

end


%
%Expiriment 5 - Bad result
%

if(xp5)
    
result = shrnk(badIm,20,20); 
imwrite(result, 'reasa.5b.jpg');
    
end
