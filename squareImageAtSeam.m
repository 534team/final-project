function [ out ] = squareImageAtSeam( fid, in, ewidth, seam )
%SQUAREIMAGEATSEAM makes a single tile using the seam
%   Detailed explanation goes here

[rows, width, ~] = size(in);

seamOffset =  width - ewidth;
seamMax = max(seam);
fprintf(fid, 'writing seam.png output image \n');
imwrite(overlay_seam(in, seam, seamOffset),'seam.png');

outWidth = width-ewidth;
half = ones(rows,1) * (seamMax+1); %floor(outWidth/2)
out = zeros(rows,outWidth,3);

a = in(:,half:1:(seam(:) + seamOffset),:);
b = in(:,(seam(:) + 1):1:(half -1),:);

out(:,:,:) = [a b];

end

