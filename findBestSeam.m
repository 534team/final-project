function [seam, ewidth, seamEnergy ]= findBestSeam( fid, in, ReduceFactor, EnergyMode, SeamMode )
%FINDBESTSEAM Summary of this function goes here
%   Detailed explanation goes here

if(strcmp(SeamMode, '8-con'))
    if(ReduceFactor == 1) 
    %loop through all possible values looking for best
        fprintf(fid,'finding optimal seam!\n');
        [~,cols,~] = size(in);
        halfw = floor(.5*cols);
        minEnergy = 999999999;
        i = 0;
        while(i<halfw-4)
            [overlapl, overlapr] = seamPartOfImage(fid, in, ReduceFactor, halfw+i);
            e = imenergy(overlapl, overlapr, EnergyMode);
            [seam, energy] = vertSeam(e);
            if(energy < minEnergy)
                minEnergy = energy;
                bestSeam = seam;
                [~, ewidth] = size(e);
                fprintf(fid,'new best seam with energy: %f\n',minEnergy);
            end
            i = i + 1;
        end
        seamEnergy = minEnergy;
        seam = bestSeam;
        return


    else

        [overlapl1, overlapr1] = seamPartOfImage(fid, in, ReduceFactor, 0);
        [overlapl2, overlapr2] = seamPartOfImage(fid, in, ReduceFactor, 1);

        e = imenergy(overlapl1, overlapr1, EnergyMode);
        e2 = imenergy(overlapl2, overlapr2, EnergyMode);

        [~   , ssd1] = vertSeam(e);
        [seam, ssd2] = vertSeam(e2);

        %find local min
        if(ssd1>ssd2)
             fprintf(fid,'We are moving our seam to the right\n');
             fprintf(fid,'val left: %f\n',ssd1);
             fprintf(fid,'val right: %f\n',ssd2);
             i=1;
             while(ssd2 <= ssd1)
                 fprintf(fid,'val: %f\n',ssd2);
                 ssd1 = ssd2;
                 oldseam = seam;
                 [overlapl, overlapr, error] = seamPartOfImage(fid, in, ReduceFactor, 1+i);
                 if(error)
                    fprintf(fid,'we moved right %d times before we hit a boundry\n', i);
                    seam = 0; [~, ewidth] = size(e); return; 
                 end
                 e = imenergy(overlapl, overlapr, EnergyMode);
                 [seam, ssd2] = vertSeam(e);
                 i = i + 1;
             end
             fprintf(fid,'we moved right %d times before we found a local min\n', i);
             fprintf(fid, 'local breaking value was: %f\n',ssd2);
             seam = oldseam;
             seamEnergy = ssd2;
        else
             fprintf(fid,'We are moving our seam to the left\n');
             fprintf(fid,'val right: %f\n',ssd2);
             fprintf(fid,'val left: %f\n',ssd1);
             i=0;
             while(ssd2 >= ssd1)
                 fprintf(fid,'val: %f\n',ssd1);
                 ssd2 = ssd1;
                 oldseam = seam;
                 [overlapl, overlapr, error] = seamPartOfImage(fid, in, ReduceFactor, i);
                 if(error)
                    fprintf(fid,'we moved left %d times before we hit a boundry\n', i);
                    seam = 0; [~, ewidth] = size(e); return; 
                 end
                 e = imenergy(overlapl, overlapr, EnergyMode);
                 [seam, ssd1] = vertSeam(e);
                 i = i - 1;
             end
             fprintf(fid,'we moved left %d times before we found a local min\n', abs(i));
             fprintf(fid, 'local breaking value was: %f\n',ssd1);
             seam = oldseam;
             seamEnergy = ssd1;
        end

    [~, ewidth] = size(e);
    return
    end
elseif(strcmp(SeamMode, 'straight'))
    if(ReduceFactor == 1) 
        %loop through all possible values looking for best
        fprintf(fid,'finding optimal seam!\n');
        [~,cols,~] = size(in);
        halfw = floor(.5*cols);
        minEnergy = 999999999;
        i = 0;
        while(i<halfw-4)
            [overlapl, overlapr] = seamPartOfImage(fid, in, ReduceFactor, halfw+i);
            e = imenergy(overlapl, overlapr, EnergyMode);
            [seam, energy] = straightSeam(e);
            if(energy < minEnergy)
                minEnergy = energy;
                bestSeam = seam;
                [~, ewidth] = size(e);
                fprintf(fid,'new best seam with energy: %f\n',minEnergy);
            end
            i = i + 1;
        end
        	seamEnergy = minEnergy;
            seam = bestSeam;
        return
    else
        fprintf(fid,'local min seam not implemented for straight seams\n');
    end
end
end

