function imgOut = overlay_seam( I, S, off )
%OVERLAY_SEAMS adds a red seam on the image

%just incase
I = im2double(I);

[rows,cols,~] = size(I);
red = I(:,:,1);
green = I(:,:,2);
blue = I(:,:,3);
i = 1;
while(i <= rows)
   red(i,S(i)) = 1;
   green(i,S(i)) = 0;
   blue(i,S(i)) = 0;
   i = i + 1;
end

i = 1;
while(i <= rows)
   red(i,S(i) + off) = 1;
   green(i,S(i) + off) = 0;
   blue(i,S(i) + off) = 0;
   i = i + 1;
end

imgOut = cat(3,red,green,blue);

end

